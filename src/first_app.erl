%%%-------------------------------------------------------------------
%%% @author marius_cornescu
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 11. Oct 2015 8:58 AM
%%%-------------------------------------------------------------------
-module(first_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%% ===================================================================
%% Application callbacks
%% ===================================================================

start(_StartType, _StartArgs) ->
    first_sup:start_link().

stop(_State) ->
    ok.
